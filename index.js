var express = require('express');
var app = express();
var port = process.env.PORT || 7777;
var bodyParser = require('body-parser');
var users = require('./users');
var mongojs = require('mongojs');

var databaseUrl = 'devahoy_mongojs';
var collections = ['users', 'clubs'];

var db = mongojs(databaseUrl, collections);

app.use(express.static(__dirname + '/public')); 
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json

var mongodb = require('mongodb');


app.get('/', function (req, res) {
    res.send('<h1>Hello Node.js</h1>');
});

app.get('/user', function (req, res) {

	db.users.find(function(err,docs){

        
		res.json(docs);
	});

    
});




app.delete('/user/:id', function (req, res) {
    var id = req.params.id;
    console.log(req.body);
    db.users.remove({_id: mongodb.ObjectId(id)},
        function(err,result){
            res.json({
                message:"ok",
                user: result
            });
    });
});

app.post('/user', function (req, res) {
    
    var json = req.body;

	db.users.insert(json);


    res.send('Add new ' + json.name + ' Completed!');
});

app.get('*', function(req, res) {
        res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});




app.listen(port, function() {
    console.log('Starting node.js on port ' + port);
});