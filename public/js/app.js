angular.module('SimpleApp',['simple.service','simple.ctrl','ngMaterial','ui.router'])
.run(['$rootScope', '$state', '$stateParams',function ($rootScope, $state,$stateParams) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

}])
.config(function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/datatable");
  $stateProvider
  .state('insert',{
    url : '/insert',
    templateUrl : 'insert.html',
    controller : 'InsertCtrl'
  })
  .state('datatable',{
    url : '/datatable',
    templateUrl : 'datatable.html',
    controller : 'DataTableCtrl'
  });
});