angular.module('simple.service',['ngResource'])
	.factory('SimpleSv',function ($resource) {
		return $resource('http://localhost:7777/user/:id',{id:'@_id'}
			,{ 
				findAll :{ 
					method: 'get' ,isArray:true 
				}
			},
			{
				deleteUser :{
					method: 'delete'
				}
			}
		);
	});