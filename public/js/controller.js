angular.module('simple.ctrl',[])
	.controller('MangeCtrl', ['$scope','$mdSidenav', function ($scope,$mdSidenav) {
		
	$scope.toggleSidenav = function(menuId) {
	    $mdSidenav(menuId).toggle();
	  };


	}])

	.controller('MenuCtrl', ['$scope', function ($scope) {
		
	}])
	.controller('InsertCtrl', ['$scope', function ($scope) {
		
	}])

	.controller('DataTableCtrl', ['$scope','SimpleSv', function ($scope,SimpleSv) {
		
		$scope.users = {};

		$scope.init = function(){
			SimpleSv.findAll().$promise.then(function(users) {
				console.log(users);
			      $scope.users = users ;
 			});
		}

		$scope.init();

		$scope.remove = function(user){
			console.log("remove :  " + user);
			//console.log(SimpleSv.deleteUser);
			SimpleSv.delete({id:user._id});
			$scope.init();


		}



	}]);

